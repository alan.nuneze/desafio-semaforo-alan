const URL = "https://xompasssuiteadminstg.z20.web.core.windows.net/semaphore.json";

function sleep(ms){
    return new Promise(resolve => setTimeout(resolve,ms));
}

const changeColor = async (color) => {

    let luces = document.querySelectorAll('.semaforo .luz');

    luces.forEach((luz) => {
        if (color === luz.dataset.color){
            luz.querySelector('span').classList.add('active');
        
        } else {
            luz.querySelector('span').classList.remove('active');
        }
    });
}


async function handler(lights) {

    for (let i = 0; i < lights.length; i++){
        let color = lights[i].color;
        let duration = lights[i].duration * 1000;
        changeColor(color);
        await sleep (duration);
    }    

}


function getData(callback) {

    fetch(URL)
    .then((response) => response.json())
    .then((semaforos) => {
        let init_color = semaforos.currentLightColor;
        let lights = semaforos.lights.reverse();
        changeColor(init_color);
        callback(null,[lights, init_color]);

    })
    .catch((e) => console.log(e));
}


getData(async function(err, content)  {

    let newLights = content[0];
    let newInit_color = content[1];
    changeColor(newInit_color);
    while (true){
        await handler(newLights);
    }
});
